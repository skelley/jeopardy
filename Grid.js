class Grid {
    constructor(columns, rows, cellSize) {
        this.columns = columns;
        this.rows = rows;
        this.cellSize = cellSize;
        this.boardElement = document.createElement('div')
        this.boardElement.id = 'board'
        this.boardElement.style.display = 'flex'
        this.cellGrid = []
        for (let columnIndex = 0; columnIndex < this.columns; columnIndex++) {
            this.columnElement = document.createElement('div')
            this.columnElement.id = 'column'+columnIndex
            this.cellGrid.push([])
            for (let rowIndex = 0; rowIndex < this.rows; rowIndex++) {
                let newCell = new Cell(this.cellSize, rowIndex, columnIndex)
                this.cellGrid[columnIndex].push(newCell)
                this.columnElement.appendChild(newCell.cellElement)
            }
            this.boardElement.appendChild(this.columnElement)
        }
    }
    updateDOM(destination, element) {
        destination.appendChild(element)
    }
    returnCell(columnIndex, rowIndex) {
        return this.cellGrid[columnIndex][rowIndex]
    }
 }
 